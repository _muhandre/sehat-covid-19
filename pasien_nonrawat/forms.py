from django import forms
from .models import PatientNonRawat

class FormPatientNonRawat(forms.ModelForm):
    class Meta:
        model = PatientNonRawat
        fields = ['patient_name', 'patient_phone', 'patient_address', 'hospital_name', 'symptoms']
        widgets = {
            'patient_name' : forms.TextInput(attrs = {'class': 'form-control post-text'}),
            'patient_phone' : forms.TextInput(attrs = {'class': 'form-control post-text'}),
            'patient_address' : forms.TextInput(attrs = {'class': 'form-control post-text'}),
            'hospital_name' : forms.TextInput(attrs = {'class': 'form-control post-text'}),
            'symptoms' : forms.Textarea(attrs = {'class': 'form-control post-text', 'rows' : 4})
        }

        error_messages = {
            'max_length' : 'Input anda terlalu panjang'
        }
    #     fields = ['patient_name', 'patient_phone', 'patient_address', 'hospital_name', 'symptoms']

    # patient_name = forms.CharField(widget = forms.TextInput(attrs = {'class' : 'form-control'})),
    # patient_phone = forms.CharField(widget = forms.TextInput(attrs = {'class' : 'form-control'})),
    # patient_address = forms.CharField(widget = forms.TextInput(attrs ={'class' : 'form-control'})),
    # hospital_name = forms.CharField(widget = forms.TextInput(attrs = {'class' : 'form-control'})),
    # symptoms = forms.CharField(widget = forms.Textarea(attrs = {'class': 'form-control', 'rows': 6}))
