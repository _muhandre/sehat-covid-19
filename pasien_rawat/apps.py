from django.apps import AppConfig


class PasienRawatConfig(AppConfig):
    name = 'pasien_rawat'
