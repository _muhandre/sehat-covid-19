from django.db import models

# Create your models here.
class ModelRS(models.Model):
    nama = models.CharField(max_length = 60)
    nohp = models.CharField(max_length = 30)
    alamat = models.CharField(max_length = 500)
    provinsi = models.CharField(max_length = 60)
