from django.http import HttpResponse
from django.shortcuts import redirect, render

# reference from : https://www.youtube.com/watch?v=eBsc65jTKvw&list=WL&index=2&ab_channel=DennisIvy

def is_logged(view_func):
    def wrapper_func(request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('/')
        else:
            return view_func(request, *args, **kwargs)
    
    return wrapper_func

def allowed(roles=[]):
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):
            group = None
            if request.user.groups.exists():
                group = request.user.groups.all()[0].name

            if group in roles:
                return view_func(request, *args, **kwargs)
            else:
                return render(request, 'authentications/notAuthorized.html', {"user_type": roles[0]})
        return wrapper_func
    return decorator