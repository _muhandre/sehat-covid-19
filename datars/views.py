from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from .models import ModelRS
from .forms import rsform
from django.contrib.auth.decorators import login_required
from authentications.decorators import allowed
from django.core import serializers
import json


# Create your views here.
@login_required(login_url='/login')
@allowed(roles=['Hospital'])
def post_datars(request): 
    if request.method == 'POST':
        form = rsform(request.POST or None)
        if form.is_valid():
            form.save()
    return redirect('/datars')

@login_required(login_url='/login')
@allowed(roles=['Hospital'])
def datars(request):
    user_data = {
        'nama' : request.user.first_name,
        'nohp' : "",
        'alamat' : "",
        'provinsi' : ""
    }
    context = {
        'form_rs': rsform(initial = user_data),
    }
    return render(request, 'datars/datars.html', context)

def detailrs_js(request, provinsi):
    rs_yang_tersedia = ModelRS.objects.filter(provinsi = provinsi)
    rs_yang_tersedia = serializers.serialize("json", rs_yang_tersedia)
    rs_yang_tersedia = json.loads(rs_yang_tersedia)
    return JsonResponse(rs_yang_tersedia,  json_dumps_params={'indent': 2}, safe=False)


@login_required(login_url='/login')
def detail_rs(request, provinsi):
    # rs_yang_tersedia = ModelRS.objects.filter(provinsi = provinsi)
    context = {
        'provinsi' : provinsi
    }
    return render(request, 'datars/detailrs.html', context)
