from django.test import TestCase
from django.urls import resolve, reverse
from .models import Testimoni
from .forms import Form_Testimoni
from . import views

class MainTestCase(TestCase):
    def setUp(self):
        patientUser_data = {
            'username'      :   "boi",
            'first_name'    :   "namanyarahasia",
            'user_type'     :   "Patient",
            'email'         :   "fasilkom@gmail.com",
            'password1'     :   "UlkasuwhLKJ1238lk",
            'password2'     :   "UlkasuwhLKJ1238lk",
        }
        self.client.post('/register/', patientUser_data)
        self.client.login(username = "boi", password = "UlkasuwhLKJ1238lk")

    def test_response_page_home(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_page_home(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response,'main/home.html')

    def test_response_page_testiform(self):
        response = self.client.get('/testiform')
        self.assertEqual(response.status_code, 200)
    
    def test_template_page_testiform(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response,'main/home.html')

    def test_response_page_testiform_acc(self):
        response = self.client.get('/testiform_acc')
        self.assertEqual(response.status_code, 200)

    def test_template_page_testiform_acc(self):
        response = self.client.get('/testiform_acc')
        self.assertTemplateUsed(response,'main/testiform_acc.html')
    
    def test_model_Testimoni(self):
        Testimoni.objects.create(display_name='boi', content='xd')
        n = Testimoni.objects.all().count()
        self.assertEqual(n,1)
    
    def test_form_Form_Testimoni(self):
        data = {'display_name':'boi', 'content':'xdd'}
        form_testimoni = Form_Testimoni(data=data)
        self.assertTrue(form_testimoni.is_valid())
        self.assertEqual(form_testimoni.cleaned_data['display_name'],'boi')
        self.assertEqual(form_testimoni.cleaned_data['content'],'xdd')

    def test_Form_Testimoni_can_save_POST_request(self):
        data = {
            'display_name':'boi', 
            'content':'xdd'
        }
        response = self.client.post(
            '/save',
            data
        )
        found = resolve('/save')
        self.assertEqual(found.func, views.save)
        n = Testimoni.objects.all().count()
        self.assertEqual(n, 1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/testiform_acc')