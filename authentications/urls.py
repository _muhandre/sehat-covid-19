from django.urls import path
from . import views

app_name = 'authentications'

urlpatterns = [
    path('register/', views.register, name = 'authentications-register'),
]
