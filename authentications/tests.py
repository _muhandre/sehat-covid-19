from django.test import TestCase
from django.contrib.auth.models import User, Group

# Create your tests here.
class AuthenticationsUnitTest(TestCase):
    def test_register_url_is_exist(self):
        response = self.client.get(
            '/register/',
        )

        self.assertEqual(response.status_code, 200)

    def test_hospital_user_register_valid_and_group_has_been_created(self):
        hospitalUser_data = {
            'username'      : "namaHospitalUseryangkitakenal",
            'first_name'    : "Nama lengkap user hospital",
            'user_type'     : "Hospital",
            'email'         : "emailcalonhospitaluser@gmail.com",
            'password1'     : "Inipasswordnya123",
            'password2'     : "Inipasswordnya123"
        }
        Group.objects.create(name = 'Hospital')
        response = self.client.post(
            '/register/',
            hospitalUser_data
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/register')
    
    def test_hospital_user_register_valid_group_has_not_been_created(self):
        hospitalUser_data = {
            'username'      : "namaHospitalUseryangkitakenal",
            'first_name'    : "Nama lengkap user hospital",
            'user_type'     : "Hospital",
            'email'         : "emailcalonhospitaluser@gmail.com",
            'password1'     : "Inipasswordnya123",
            'password2'     : "Inipasswordnya123"
        }
        response = self.client.post(
            '/register/',
            hospitalUser_data
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/register')
    
    def test_patient_user_register_valid_group_has_been_created(self):
        patientUser_data = {
            'username'      : "namapatientUseryangkitakenal",
            'first_name'    : "Nama lengkap user patient",
            'user_type'     : "Patient",
            'email'         : "emailcalonpatientuser@gmail.com",
            'password1'     : "Inipasswordnya123",
            'password2'     : "Inipasswordnya123"
        }
        Group.objects.create(name = 'Patient')
        response = self.client.post(
            '/register/',
            patientUser_data
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/register')

    def test_patient_user_register_valid_group_has_not_been_created(self):
        patientUser_data = {
            'username'      : "namapatientUseryangkitakenal",
            'first_name'    : "Nama lengkap user patient",
            'user_type'     : "Patient",
            'email'         : "emailcalonpatientuser@gmail.com",
            'password1'     : "Inipasswordnya123",
            'password2'     : "Inipasswordnya123"
        }
        response = self.client.post(
            '/register/',
            patientUser_data
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/register')
    
    def test_user_register_invalid(self):
        patientUser_data = {
            'username'      : "namapatientUseryangkitakenal",
            'first_name'    : "Nama lengkap user patient",
            'user_type'     : "Patient",
            'email'         : "emailcalonpatientuser@gmail.com",
            'password1'     : "Inipasswordnya123",
            'password2'     : "Inipasswordnya12"
        }
        response = self.client.post(
            '/register/',
            patientUser_data
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/register')