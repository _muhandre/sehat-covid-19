from django.contrib import admin
from django.urls import path
from .views import index, submit_form

app_name = 'pasien_rawat'

urlpatterns = [
    path('', index, name='pasien_rawat-index'),
    path('submitform', submit_form, name='pasien_rawat-submitform'),
]
