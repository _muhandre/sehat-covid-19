from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.core import serializers
from datars.models import ModelRS
from .forms import PasienRawatForm
from django.contrib.auth.decorators import login_required
from authentications.decorators import allowed

# Create your views here.
@login_required(login_url='/login')
@allowed(roles=['Patient'])
def index(request):
    rs_yang_tersedia = ModelRS.objects.all()
    provinsi_yang_tersedia = []
    for rs in rs_yang_tersedia:
        if rs.provinsi not in provinsi_yang_tersedia:
            provinsi_yang_tersedia.append(rs.provinsi)
    user_data = {
        'nama' : request.user.first_name,
        'nomor_telepon' : "",
        'alamat' : "",
        'nama_rumah_sakit' : "",
        'dokumen_hasil_test' : None,
    }
    context = {
        'pasien_rawat_form' : PasienRawatForm(initial = user_data),
        'provinsi_yang_tersedia' : provinsi_yang_tersedia
    }
    return render(request, "pasien_rawat/index.html", context)

@login_required(login_url='/login')
@allowed(roles=['Patient'])
def submit_form(request):
    import base64
    ada_rumah_sakitnya = False
    
    if request.is_ajax and request.method == "POST":
        form = PasienRawatForm(request.POST, request.FILES)
        if form.is_valid():
            pasien_rawat = form.save(commit=False)
            with form.cleaned_data["dokumen_hasil_test"].open('rb') as file:
                pasien_rawat.base64_doc = "data:image/png;base64,{}".format(base64.b64encode(file.read()).decode())
                semua_rs = ModelRS.objects.all()
                for rs in semua_rs:
                    if rs.nama == request.POST["nama_rumah_sakit"]:
                        pasien_rawat.rumah_sakit = rs
                        pasien_rawat.save()
                        ada_rumah_sakitnya = True
                        break
            if ada_rumah_sakitnya:
                pasienRawat_serialized = serializers.serialize('json', [ pasien_rawat, ])
                return JsonResponse({"pasien_rawat" :   pasienRawat_serialized}, status = 200)
            else:
                return JsonResponse({"error" : form.errors}, status = 400)
        else:
            return JsonResponse({"error" : form.errors}, status = 400)
    return HttpResponseRedirect('/pasienrawat/')