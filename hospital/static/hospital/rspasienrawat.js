$(document).ready(
    function(){
        $.ajax({
            url     :   '/hospital/getrspasienrawat',
            type    :   'GET',
            success :   function(response) {
                for(var namaRs of response.list_nama_rs) {
                    $('.js-list-nama-rs').append(
                        "<div class = 'color-1'>" +
                            "<a href='" + namaRs + "/daftarpasien/'>" +
                                "<p class = 'montserrat-regular small-font padding-2 margin-left-1'>" + namaRs + "</p>" +
                            "</a>" +
                        "</div>"
                    )
                }
            }
        })
    }
);
