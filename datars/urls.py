from django.urls import path
from . import views

app_name = "datars"

urlpatterns = [
    path('', views.datars, name = 'datars-index'),
    path('post', views.post_datars, name='post'),
    path('js/<str:provinsi>/', views.detailrs_js, name = "datars-detailrs_js"),
    path('<str:provinsi>/', views.detail_rs, name = "datars-detailrs")
]