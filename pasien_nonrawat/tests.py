from django.test import TestCase
from django.urls import resolve
from .models import PatientNonRawat
from datars.models import ModelRS
from . import views
from django.contrib.auth.models import User, Group
from authentications.forms import CreateForm

# Create your tests here.
""" class UnitTestPasieNonRawat(TestCase):
    def setUp(self):
        hospitalUser_data = {
            'username'      :   "Nama rumah sakit",
            'first_name'    :   "namanyarahasia",
            'user_type'     :   "Hospital",
            'email'         :   "fasilkom@gmail.com",
            'password1'     :   "UlkasuwhLKJ1238lk",
            'password2'     :   "UlkasuwhLKJ1238lk",
        }
        self.client.post('/register/', hospitalUser_data)
        self.client.login(username = "Nama rumah sakit", password = "UlkasuwhLKJ1238lk")
        rs_data = {
            'nama' : "Nama rumah sakit",
            'nohp' : "012345678901",
            'alamat' : "Alamat rumah sakit",
            'provinsi' : "Provinsi rumah sakit"
        }
        self.client.post('/datars/post', rs_data)
        self.client.logout()
        patientUser_data = {
            'username'      :   "reisestory1212",
            'first_name'    :   "namanyarahasia",
            'user_type'     :   "Patient",
            'email'         :   "fasilkom@gmail.com",
            'password1'     :   "UlkasuwhLKJ1238lk",
            'password2'     :   "UlkasuwhLKJ1238lk",
        }
        self.client.post('/register/', patientUser_data)
        self.client.login(username = "reisestory1212", password = "UlkasuwhLKJ1238lk")
        pasienNonrawat_data = {
            'patient_name' : 'reisestory1212',
            'patient_phone' : 'test_telpon',
            'patient_address' : 'test_alamat',
            'hospital_name' : "Nama rumah sakit",
            'symptoms' : 'gejala_penyakit',
            'hospital' : ModelRS.objects.filter(nama = "Nama rumah sakit")
        }
        self.client.post('/pasiennonrawat/savepatient', pasienNonrawat_data)

    def test_model(self):
        jumlah_semua_pasiennonrawat = PatientNonRawat.objects.all().count()
        # self.assertEqual(jumlah_semua_pasiennonrawat, 1)

    def test_webpage_response(self):
        response = self.client.get('/pasiennonrawat/')
        self.assertEqual(response.status_code, 200)

    def test_function_used(self):
        found = resolve('/pasiennonrawat/')
        self.assertEqual(found.func, views.pasiennonrawat)

    def test_template(self):
        response = self.client.get('/pasiennonrawat/')
        self.assertTemplateUsed(response, 'pasien_nonrawat/pasiennonrawat.html')

    def test_show_available_hospital(self):
        rs = ModelRS.objects.create(
            nama = "Nama rumah sakit 2",
            nohp = "012345678901",
            alamat = "Alamat rumah sakit 2",
            provinsi = "Provinsi rumah sakit 2"
        )
        response = self.client.get('/pasiennonrawat/')
        html_response = response.content.decode('utf8')
        self.assertIn("Provinsi rumah sakit", html_response)
        self.assertIn("Provinsi rumah sakit 2", html_response)

    def test_texts(self):
        response = self.client.get('/pasiennonrawat/')
        content = response.content.decode('utf8')
        self.assertIn("Data Diri Pasien" , content)
        self.assertIn("Nama", content)
        self.assertIn("No Telepon / HP", content)
        self.assertIn("Alamat", content)
        self.assertIn("RS", content)
        self.assertIn("Gejala", content)

    def test_views(self):
        response = self.client.post('/pasiennonrawat/savepatient', 
            data={
                'patient_name' : 'test_pasien 2',
                'patient_phone' : 'test_telpon 2',
                'patient_address' : 'test_alamat 2',
                'hospital_name' : 'Nama rumah sakit',
                'symptoms' : 'gejala_penyakit 2'
            }
        )
        found = resolve('/pasiennonrawat/savepatient')
        self.assertEqual(found.func, views.savepatient)
        jumlah_semua_pasiennonrawat = PatientNonRawat.objects.all().count()
        # self.assertEquals(jumlah_semua_pasiennonrawat, 2)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response['location'], '/pasiennonrawat/') """