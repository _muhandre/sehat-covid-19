from django.conf.urls import url
from django.urls import path
from .views import *

app_name = 'hospital'

urlpatterns = [
	path('', index, name = 'hospital-index'),
	path('rspasienrawat/', rspasienrawat, name = 'hospital-rspasienrawat'),
	path('getrspasienrawat/', getrspasienrawat, name = 'hospital-getrspasienrawat'),
	path('rspasienrawat/<str:nama_rs>/daftarpasien/', daftarpasienrawat, name = 'hospital-daftarpasienrawat'),
	path('rspasienrawat/<str:nama_rs>/daftarpasien/getdaftarpasien', getdaftarpasienrawat, name = 'hospital-getdaftarpasienrawat'),
	
	path('rspasienperiksa/', rspasienperiksa, name= 'hospital-rspasienperiksa'),
	path('getrspasienperiksa/', getrspasienrawat, name = 'hospital-getrspasienrawat'),
	path('rspasienperiksa/<str:nama_rs>/daftarpasien/', daftarpasienperiksa, name= 'hospital-daftarpasienperiksa'),
	path('rspasienrawat/<str:nama_rs>/daftarpasien/getdaftarpasien', getdaftarpasienrawat, name = 'hospital-getdaftarpasienrawat'),
]
