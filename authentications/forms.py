from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class CreateForm(UserCreationForm):
    class Meta:
        model = User
        fields = ["username", "first_name", "user_type", "email", "password1", "password2"]
    
    type_choices =[("Patient", "Patient"), ("Hospital", "Hospital")]
    user_type = forms.ChoiceField(choices = type_choices, label = "Jenis user")