$(document).ready(
    function(){
        $('.js-back-button').each(function() {
            $(this).hover(function() {
                $(this).attr("src", "/static/hospital/svg/back_over.svg")
            }) (function() {
                $(this).attr("src", "/static/hospital/svg/back.svg")
            })
        })

        var startPos = window.location.href.lastIndexOf("rspasienperiksa")
        startPos += 14
        var endPos = window.location.href.lastIndexOf('/daftarpasien')
        var namaRs = window.location.href.substring(startPos, endPos)
        $.ajax({
            url     :   "/hospital/rspasienperiksa/" + namaRs + "/daftarpasien/getdaftarpasien",
            type    :   'GET',
            success :   function(response) {
                console.log(response)
                data = response.hospital_patient
                for(var namaPasienRawatRsIni of data) {
                    $('.js-patient-list').append(
                        "<table id = 'table' class = 'full-size margin-top-2'>" +
                            "<tr class = 'tabel-border color-1'>" +
                                "<th>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-1 margin-left-4'>Pasien</p>" +
                                "</th>" +
                                "<th></th>" +
                            "</tr>" +
                            "<tr>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>Nama</p>" +
                                "</th>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>" +  namaPasienRawatRsIni.patient_name + "</p>" +
                                "</th>" +
                            "</tr>" +
                            "<tr>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>No telepon / HP</p>" +
                                "</th>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>" + namaPasienRawatRsIni.patient_phone + "</p>" +
                                "</th>" +
                            "</tr>" +
                            "<tr>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>Alamat</p>" +
                                "</th>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>" + namaPasienRawatRsIni.patient_address + "</p>" +
                                "</th>" +
                            "</tr>" +
                            "<tr>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>Gejala</p>" +
                                "</th>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>" + namaPasienRawatRsIni.symptoms + "</p>" +
                                "</th>" +
                            "</tr>" +
                        "</table>"
                    )
                }
            },
            error   :   function(response) {
            }
        })
    }
);
