asgiref==3.2.10
coverage==5.3
dj-database-url==0.5.0
Django==3.1.4
django-crispy-forms==1.10.0
gunicorn==20.0.4
Pillow==8.0.1
psycopg2-binary==2.8.6
pytz==2020.1
sqlparse==0.4.1
whitenoise==5.2.0
