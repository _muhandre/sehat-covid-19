from django import forms
from .models import Testimoni

class Form_Testimoni(forms.ModelForm):
    class Meta:
        model = Testimoni
        fields = ['display_name','content']
        
    error_messages = {
        'required' : 'This Field is Required'
    }
    input_attrs = {
        'type' : 'text',
        'placeholder' : 'Nama',
        'class' : 'form-control'
    }
    input_attrs1 = {
        'type' : 'text',
        'placeholder' : 'Kesan',
        'class' : 'form-control',
    }

    display_name = forms.CharField(label='', required=True, max_length=35, widget=forms.TextInput(attrs=input_attrs))
    content = forms.CharField(label='', required=True, max_length = 500, widget=forms.Textarea(attrs=input_attrs1))