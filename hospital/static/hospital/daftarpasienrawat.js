$(document).ready(
    function(){
        $('.js-back-button').each(function() {
            $(this).mouseenter(function() {
                $(this).attr("src", "/static/hospital/svg/back_over.svg")
            })
            $(this).mouseleave(function() {
                $(this).attr("src", "/static/hospital/svg/back.svg")
            })
        })

        var startPos = window.location.href.lastIndexOf("rspasienrawat")
        startPos += 14
        var endPos = window.location.href.lastIndexOf('/daftarpasien')
        var namaRs = window.location.href.substring(startPos, endPos)
        $.ajax({
            url     :   "/hospital/rspasienrawat/" + namaRs + "/daftarpasien/getdaftarpasien",
            type    :   'GET',
            success :   function(response) {
                console.log(response)
                for(var namaPasienRawatRsIni of response.pasiennya_nama_rs) {
                    $('.js-list-nama-pasien').append(
                        "<table id = 'table' class = 'full-size margin-top-2'>" +
                            "<tr class = 'tabel-border color-1'>" +
                                "<th>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-1 margin-left-4'>Pasien</p>" +
                                "</th>" +
                                "<th></th>" +
                            "</tr>" +
                            "<tr>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>Nama</p>" +
                                "</th>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>" +  namaPasienRawatRsIni.nama + "</p>" +
                                "</th>" +
                            "</tr>" +
                            "<tr>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>No telepon / HP</p>" +
                                "</th>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>" + namaPasienRawatRsIni.nomor_telepon + "</p>" +
                                "</th>" +
                            "</tr>" +
                            "<tr>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>Alamat</p>" +
                                "</th>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>" + namaPasienRawatRsIni.alamat + "</p>" +
                                "</th>" +
                            "</tr>" +
                            "<tr>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<p id = 'desc-text' class = 'montserrat-regular medium-font padding-half margin-left-4'>Dokumen hasil test</p>" +
                                "</th>" +
                                "<th class = 'tabel-border' style = 'width: 50%'>" +
                                    "<img class = 'padding-2 margin-left-3' style = 'width: 80%' src='" + namaPasienRawatRsIni.base64_doc + "' alt='Dokumen pasien rawat'>" +
                                "</th>" +
                            "</tr>" +
                        "</table>"
                    )
                }
            },
            error   :   function(response) {
            }
        })
    }
);
