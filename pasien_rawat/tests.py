from django.test import TestCase
from django.urls import resolve
from .views import index, submit_form
from .models import PasienRawat
from datars.models import ModelRS
from django.core.files.uploadedfile import SimpleUploadedFile
import base64
from django.contrib.auth.models import User, Group
from authentications.forms import CreateForm

# Create your tests here.
class PasienRawatUnitTest(TestCase):
    def setUp(self):
        hospital1User_data = {
            'username'      :   "hospital1",
            'first_name'    :   "Nama rumah sakit",
            'user_type'     :   "Hospital",
            'email'         :   "fasilkom@gmail.com",
            'password1'     :   "UlkasuwhLKJ1238lk",
            'password2'     :   "UlkasuwhLKJ1238lk",
        }
        self.client.post('/register/', hospital1User_data)
        self.client.login(username = "hospital1", password = "UlkasuwhLKJ1238lk")
        hospital1_data = {
            'nama'      :   'Nama rumah sakit',
            'nohp'      :   '012345678901',
            'alamat'    :   'Alamat rumah sakit',
            'provinsi'  :   'Jawa Barat'
        }
        self.client.post('/datars/post', hospital1_data)
        self.client.logout()

        hospital2User_data = {
            'username'      :   "hospital2",
            'first_name'    :   "Nama rumah sakit 2",
            'user_type'     :   "Hospital",
            'email'         :   "fasilkom@gmail.com",
            'password1'     :   "UlkasuwhLKJ1238lk",
            'password2'     :   "UlkasuwhLKJ1238lk",
        }
        self.client.post('/register/', hospital2User_data)
        self.client.login(username = "hospital2", password = "UlkasuwhLKJ1238lk")
        hospital2_data = {
            'nama'      :   'Nama rumah sakit 2',
            'nohp'      :   '012345678901',
            'alamat'    :   'Alamat rumah sakit 2',
            'provinsi'  :   'Jawa Tengah'
        }
        self.client.post('/datars/post', hospital2_data)
        self.client.logout()
        
        patientUser_data = {
            'username'      :   "reisestory1212",
            'first_name'    :   "namanyarahasia",
            'user_type'     :   "Patient",
            'email'         :   "fasilkom@gmail.com",
            'password1'     :   "UlkasuwhLKJ1238lk",
            'password2'     :   "UlkasuwhLKJ1238lk",
        }
        self.client.post('/register/', patientUser_data)
        self.client.login(username = "reisestory1212", password = "UlkasuwhLKJ1238lk")
    
    def test_pasien_rawat_index_url_is_exist(self):
        response = self.client.get('/pasienrawat/')
        self.assertEqual(response.status_code, 200)
        
    def test_pasien_rawat_index_url_using_index_func(self):
        found = resolve('/pasienrawat/')
        self.assertEqual(found.func, index)
    
    def test_pasien_rawat_index_url_using_index_template(self):
        response = self.client.get('/pasienrawat/')
        self.assertTemplateUsed(response, 'pasien_rawat/index.html')
    
    def test_pasien_rawat_index_url_show_available_hospital(self):
        response = self.client.get('/pasienrawat/')
        html_response = response.content.decode('utf8')
        self.assertIn("Jawa Barat", html_response)
        self.assertIn("Jawa Tengah", html_response)

    def test_PasienRawat_model_can_create_new_pasienrawat(self):
        dokumen = SimpleUploadedFile(name = "test_image.jpg", content = open('media/images/test_image.jpg', 'rb').read(), content_type = 'image/jpeg')

        pasienrawat = PasienRawat.objects.create(
            nama = "reisestory1212",
            nomor_telepon = "012345678901",
            alamat = "Alamat pasien rawat",
            dokumen_hasil_test = dokumen,
            base64_doc = "data:image/png;base64,{}".format(base64.b64encode(dokumen.read()).decode()),
            nama_rumah_sakit = "Nama rumah sakit",
            rumah_sakit = ModelRS.objects.get(nama = "Nama rumah sakit")
        )
        jumlah_semua_pasienrawat = PasienRawat.objects.all().count()
        self.assertEqual(jumlah_semua_pasienrawat, 1)
    
    def test_submitform_use_certain_function(self):
        response = self.client.get(
            '/pasienrawat/submitform',
        )
        found = resolve('/pasienrawat/submitform')
        self.assertEqual(found.func, submit_form)

    def test_PasienRawatForm_can_save_valid_POST_request_with_existed_hospital(self):
        dokumen = SimpleUploadedFile(name = "test_image.jpg", content = open('media/images/test_image.jpg', 'rb').read(), content_type = 'image/jpeg')
        data = {
            'nama' : "reisestory1212",
            'nomor_telepon' : "012345678901",
            'alamat' : "Alamat pasien rawat",
            'nama_rumah_sakit' : "Nama rumah sakit",
            'dokumen_hasil_test' : dokumen 
        }
        response = self.client.post(
            '/pasienrawat/submitform',
            data
        )
        jumlah_semua_pasien_rawat = PasienRawat.objects.all().count()
        self.assertEqual(jumlah_semua_pasien_rawat, 1)
        self.assertEqual(response.status_code, 200)

    def test_PasienRawatForm_cant_save_valid_POST_request_with_nonexisted_hospital(self):
        dokumen = SimpleUploadedFile(name = "test_image.jpg", content = open('media/images/test_image.jpg', 'rb').read(), content_type = 'image/jpeg')
        data = {
            'nama' : "reisestory1212",
            'nomor_telepon' : "012345678901",
            'alamat' : "Alamat pasien rawat",
            'nama_rumah_sakit' : "Nama rumah sakit 999",
            'dokumen_hasil_test' : dokumen 
        }
        response = self.client.post(
            '/pasienrawat/submitform',
            data
        )
        jumlah_semua_pasien_rawat = PasienRawat.objects.all().count()
        self.assertEqual(jumlah_semua_pasien_rawat, 0)
        self.assertEqual(response.status_code, 400)
    
    def test_PasienRawatForm_cant_save_invalid_POST_request(self):
        data = {
            'nama' : "reisestory1212",
            'nomor_telepon' : "012345678901",
            'alamat' : "Alamat pasien rawat",
            'nama_rumah_sakit' : "Nama rumah sakit"
        }
        response = self.client.post(
            '/pasienrawat/submitform',
            data
        )
        jumlah_semua_pasien_rawat = PasienRawat.objects.all().count()
        self.assertEqual(jumlah_semua_pasien_rawat, 0)
        self.assertEqual(response.status_code, 400)
    
    def test_submitform_with_GET_request(self):
        response = self.client.get(
            '/pasienrawat/submitform',
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/pasienrawat/')