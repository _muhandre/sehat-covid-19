from django.shortcuts import render
from datars.models import ModelRS
from .forms import FormPatientNonRawat
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth.decorators import login_required
from authentications.decorators import allowed
from django.core import serializers

# Create your views here.
@login_required(login_url='/login')
@allowed(roles=['Patient'])
def pasiennonrawat(request):
    rs_yang_tersedia = ModelRS.objects.all()
    provinsi_yang_tersedia = []
    for rs in rs_yang_tersedia:
        if rs.provinsi not in provinsi_yang_tersedia:
            provinsi_yang_tersedia.append(rs.provinsi)
    user_data = {
        'patient_name' : request.user.first_name,
        'patient_phone' : "",
        'patient_address' : "",
        'hospital_name' : "",
        'symptoms' : ""
    }
    context = { 
        'pasien_nonrawat_form' : FormPatientNonRawat(initial = user_data),
        'provinsi_yang_tersedia' : provinsi_yang_tersedia
        }
    return render(request, "pasien_nonrawat/pasiennonrawat.html", context)

# Handle the form submition using ajax
# source : https://www.pluralsight.com/guides/work-with-ajax-django
@login_required(login_url='/login')
@allowed(roles=['Patient'])
def savepatient(request):
    hospital_check = False

    if request.is_ajax and request.method == "POST":
        form = FormPatientNonRawat(request.POST)
        if form.is_valid():
            pasien_nonrawat = form.save(commit=False)
            semua_rs = ModelRS.objects.all()
            for rs in semua_rs:
                if rs.nama == request.POST["hospital_name"]:
                    pasien_nonrawat.hospital = rs
                    pasien_nonrawat.save()
                    hospital_check = True
                    break
            if hospital_check:
                ser_pasien_nonrawat = serializers.serialize('json', [pasien_nonrawat, ])
                return JsonResponse({"pasien_nonrawat" : ser_pasien_nonrawat}, status = 200)
            else:
                return JsonResponse({"error" : "Hospital not found"}, status = 400)
        else:
            return JsonResponse({"error" : form.errors}, status = 400)
    return HttpResponseRedirect('/pasiennonrawat/')