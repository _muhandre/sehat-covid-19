from django.test import TestCase
from django.urls import resolve
from .views import index, rspasienrawat, rspasienperiksa, daftarpasienrawat, daftarpasienperiksa, getrspasienrawat, getdaftarpasienrawat
from datars.models import ModelRS
from pasien_rawat.models import PasienRawat
from pasien_nonrawat.models import PatientNonRawat
from django.core.files.uploadedfile import SimpleUploadedFile
import base64
from django.contrib.auth.models import User, Group
from authentications.forms import CreateForm

# Create your tests here.
class HospitalUnitTest(TestCase):
    def setUp(self):
        hospital1User_data = {
            'username'      :   "reisestory1212hospital",
            'first_name'    :   "Nama rumah sakit",
            'user_type'     :   "Hospital",
            'email'         :   "fasilkom@gmail.com",
            'password1'     :   "UlkasuwhLKJ1238lk",
            'password2'     :   "UlkasuwhLKJ1238lk",
        }
        self.client.post('/register/', hospital1User_data)
        patientUser_data = {
            'username'      :   "reisestory1212",
            'first_name'    :   "namanyarahasia",
            'user_type'     :   "Patient",
            'email'         :   "fasilkom@gmail.com",
            'password1'     :   "UlkasuwhLKJ1238lk",
            'password2'     :   "UlkasuwhLKJ1238lk",
        }
        self.client.post('/register/', patientUser_data)
        self.client.login(username = "reisestory1212hospital", password = "UlkasuwhLKJ1238lk")
        hospital1_data = {
            'nama'      :   "Nama rumah sakit",
            'nohp'      :   "012345678901",
            'alamat'    :   "Alamat rumah sakit",
            'provinsi'  :   "Jawa Barat",
        }
        self.client.post('/datars/post', hospital1_data)
        self.client.logout()
        self.client.login(username = "reisestory1212", password = "UlkasuwhLKJ1238lk")
        pasienNonrawat_data = {
            'patient_name' : 'namanyarahasia',
            'patient_phone' : '012345678901',
            'patient_address' : 'Alamat pasien pemeriksaan',
            'hospital_name' : "Nama rumah sakit",
            'symptoms' : 'gejala_penyakit',
        }
        self.client.post('/pasiennonrawat/savepatient', pasienNonrawat_data)
        dokumen = SimpleUploadedFile(name = "test_image.jpg", content = open('media/images/test_image.jpg', 'rb').read(), content_type = 'image/jpeg')
        pasienRawat_data = {
            'nama' : "namanyarahasia",
            'nomor_telepon' : "012345678901",
            'alamat' : "Alamat pasien rawat",
            'nama_rumah_sakit' : "Nama rumah sakit",
            'dokumen_hasil_test' : dokumen
        }
        self.client.post('/pasienrawat/submitform', pasienRawat_data)
        self.client.logout()
        self.client.login(username = "reisestory1212hospital", password = "UlkasuwhLKJ1238lk")

    def test_hospital_index_url_is_exist(self):
        response = self.client.get('/hospital/')
        self.assertEqual(response.status_code, 200)
    
    def test_hospital_index_url_using_index_func(self):
        found = resolve('/hospital/')
        self.assertEqual(found.func, index)
    
    def test_hospital_index_url_using_hospital_template(self):
        response = self.client.get('/hospital/')
        self.assertTemplateUsed(response, 'hospital/hospital.html')

    def test_hospital_rspasienrawat_url_is_exist(self):
        response = self.client.get('/hospital/rspasienrawat/')
        self.assertEqual(response.status_code, 200)
    
    def test_hospital_rspasienrawat_url_using_rspasienrawat_func(self):
        found = resolve('/hospital/rspasienrawat/')
        self.assertEqual(found.func, rspasienrawat)
    
    def test_hospital_rspasienrawat_url_using_rspasienrawat_template(self):
        response = self.client.get('/hospital/rspasienrawat/')
        self.assertTemplateUsed(response, 'hospital/rspasienrawat.html')

    def test_hospital_getrspasienrawat_url_with_ajax(self):
        response = self.client.get('/hospital/getrspasienrawat/')
        self.assertEqual(response.status_code, 200)
    
    def test_hospital_getrspasienrawat_json_response(self):
        response = self.client.get('/hospital/getrspasienrawat/')
        json_response = response.content.decode('utf8')
        self.assertIn("Nama rumah sakit", json_response)
    
    def test_hospital_getrspasienrawat_url_wihtout_ajax(self):
        response = self.client.post('/hospital/getrspasienrawat/')
        self.assertEqual(response.status_code, 400)

    def test_hospital_getrspasienrawat_url_using_certain_function(self):
        found = resolve('/hospital/getrspasienrawat/')
        self.assertEqual(found.func, getrspasienrawat)

    """ def test_hospital_rspasienperiksa(self):
        response = self.client.get('/hospital/rspasienperiksa/')
        self.assertEqual(response.status_code, 200)
        found = resolve('/hospital/rspasienperiksa/')
        self.assertEqual(found.func, rspasienperiksa)
        self.assertTemplateUsed(response, 'hospital/rspasienperiksa.html')
        html_response = response.content.decode('utf8')
        self.assertIn("Nama rumah sakit", html_response) """
    
    def test_hospita_daftarpasienrawat_url_using_certain_function(self):
        found = resolve('/hospital/rspasienrawat/Nama%20rumah%20sakit/daftarpasien/')
        self.assertEqual(found.func, daftarpasienrawat)
        
    def test_hospital_daftarpasienrawat_url_show_its_patient(self):
        response = self.client.get('/hospital/rspasienrawat/Nama%20rumah%20sakit/daftarpasien/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'hospital/daftarpasienrawat.html')

    def test_hospital_daftarpasienrawat_show_another_patient(self):
        response = self.client.get('/hospital/rspasienrawat/Ini%20rumah%20sakit/daftarpasien/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'hospital/rsnotallowed.html')
        html_response = response.content.decode('utf8')
        self.assertIn("Maaf", html_response)
    
    def test_hospital_getdaftarpasienrawat_url_create_ajax_and_get_request(self):
        response = self.client.get('/hospital/rspasienrawat/Nama%20rumah%20sakit/daftarpasien/getdaftarpasien')
        self.assertEqual(response.status_code, 200)

    def test_hospital_getdaftarpasienrawat_url_json_response(self):
        response = self.client.get('/hospital/rspasienrawat/Nama%20rumah%20sakit/daftarpasien/getdaftarpasien')
        json_response = response.content.decode('utf8')
        self.assertIn("namanyarahasia", json_response)
    
    def test_hospital_getdaftarpasienrawat_url_using_certain_function(self):
        found = resolve('/hospital/rspasienrawat/Nama%20rumah%20sakit/daftarpasien/getdaftarpasien')
        self.assertEqual(found.func, getdaftarpasienrawat)
    
    def test_hospital_getdaftarpasienrawat_url_post_request(self):
        response = self.client.post('/hospital/rspasienrawat/Nama%20rumah%20sakit/daftarpasien/getdaftarpasien')
        self.assertEqual(response.status_code, 400)
    
    """ def test_hospital_daftarpasienperiksa(self):
        response = self.client.get('/hospital/rspasienperiksa/Nama%20rumah%20sakit/daftarpasien/')
        self.assertEqual(response.status_code, 200)
        found = resolve('/hospital/rspasienperiksa/Nama%20rumah%20sakit/daftarpasien/')
        self.assertEqual(found.func, daftarpasienperiksa)
        self.assertTemplateUsed(response, 'hospital/daftarpasienperiksa.html')
        html_response = response.content.decode('utf8')
        self.assertIn("namanyarahasia", html_response) """