from django.urls import path
from . import views

app_name = 'pasien_nonrawat'

urlpatterns = [
    path('', views.pasiennonrawat, name='pasien_nonrawat-index'),
    path('savepatient', views.savepatient, name='pasien_nonrawat-savepatient'),
    #path('patientnonrawat-data', views.datapatient, name='pasien_nonrawat-data')
]
