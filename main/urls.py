from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='main-home'),
    path('save',views.save),
    path('testiform', views.testiform, name='testiform'),
    path('testiform_acc', views.testiform_acc, name='testiform_acc')
    
]
