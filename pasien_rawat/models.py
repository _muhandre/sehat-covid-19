from django.db import models
from datars.models import ModelRS

class PasienRawat(models.Model):
    nama = models.CharField(max_length = 60)
    nomor_telepon = models.CharField(max_length = 20)
    alamat = models.CharField(max_length = 500)
    nama_rumah_sakit = models.CharField(max_length = 60)
    dokumen_hasil_test = models.ImageField(upload_to='images', blank = False)
    base64_doc = models.TextField(null=True)
    rumah_sakit = models.ForeignKey(ModelRS, on_delete = models.CASCADE)
