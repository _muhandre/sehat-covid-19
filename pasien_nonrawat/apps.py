from django.apps import AppConfig


class PasienNonrawatConfig(AppConfig):
    name = 'pasien_nonrawat'
