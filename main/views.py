from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Form_Testimoni
from .models import Testimoni
from django.contrib.auth.decorators import login_required
from authentications.decorators import allowed

def home(request):
    response = {'input_form':Form_Testimoni}
    testimonis = Testimoni.objects.all()
    response['testimonis'] = testimonis
    return render(request, 'main/home.html', response)

@login_required(login_url='/login')
@allowed(roles=['Patient'])
def testiform(request):
    user_data = {
        'display_name' : request.user.username,
        'content' : ""
    }
    response = {
        'input_form' : Form_Testimoni(initial = user_data)
    }
    return render(request, 'main/testiform.html', response)

@login_required(login_url='/login')
@allowed(roles=['Patient'])
def testiform_acc(request):
     return render(request, 'main/testiform_acc.html')

@login_required(login_url='/login')
@allowed(roles=['Patient'])
def save(request):
    if(request.method == 'POST'):
        form = Form_Testimoni(request.POST or None)
        if (form.is_valid):
            form.save()
            return HttpResponseRedirect('/testiform_acc')
        else:
            return HttpResponseRedirect('/testiform')
    else:
        return HttpResponseRedirect('')