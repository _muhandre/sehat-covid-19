from django.test import TestCase
from django.urls import resolve, reverse
from .models import ModelRS
from . import views
from django.contrib.auth.models import User, Group
from authentications.forms import CreateForm

# Create your tests here.

class UnitTestForCatatan(TestCase):
    def setUp(self):
        hospitalUser_data = {
            'username'      :   "Nana",
            'first_name'    :   "Nama rumah sakit",
            'user_type'     :   "Hospital",
            'email'         :   "fasilkom@gmail.com",
            'password1'     :   "UlkasuwhLKJ1238lk",
            'password2'     :   "UlkasuwhLKJ1238lk",
        }
        self.client.post('/register/', hospitalUser_data)
        self.client.login(username = "Nana", password = "UlkasuwhLKJ1238lk")

    def test_response_page(self):
        response = self.client.get('/datars/')
        self.assertEqual(response.status_code, 200)

    def test_template_used_for_begin(self):
        response = self.client.get('/datars/')
        self.assertTemplateUsed(response, 'datars/datars.html')

    def test_func_page(self):
        found = resolve('/datars/') 
        self.assertEqual(found.func, views.datars)

    def test_tulisan_atas_page(self):
        response = self.client.get('/datars/')
        isi_html_atas = response.content.decode('utf8')
        self.assertIn("Data Rumah Sakit", isi_html_atas)
    
    def test_tulisan(self):
        response = self.client.get('/datars/')
        isi_html_atas = response.content.decode('utf8')
        self.assertIn("Nama", isi_html_atas)
        self.assertIn("No Telepon / HP", isi_html_atas)
        self.assertIn("Alamat", isi_html_atas)

    def test_post(self):
        response = self.client.post(reverse("datars:post"), {
            'nama': 'Nana',
            'nohp': '0888888',
            'alamat': 'jl.indah',
            'provinsi': 'Aceh',
        }, follow = True)
        self.assertEqual(response.status_code, 200)

    def test_detail(self):
        self.client.post(reverse("datars:post"), {
            'nama': 'Nana',
            'nohp': '0888888',
            'alamat': 'jl.indah',   
            'provinsi': 'Aceh',
        }, follow=True)
        response = self.client.get(reverse("datars:datars-detailrs", args=['Aceh']))
        self.assertEqual(response.status_code, 200)
