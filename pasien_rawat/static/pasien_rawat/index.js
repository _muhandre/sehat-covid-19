$(document).ready(
    function(){
        $('.js-submit-button').each(function() {
            $(this).mouseenter(function() {
                $(this).attr("src", "/static/pasien_rawat/svg/submit_over.svg")
            })
            $(this).mouseleave(function() {
                $(this).attr("src", "/static/pasien_rawat/svg/submit.svg")
            })
        })
        $('.js-upload-button-div').each(function() {
            $(this).mouseenter(function() {
                $(this).find('.js-upload-button').attr("src", "/static/pasien_rawat/svg/upload_over.svg")
            })
            $(this).mouseleave(function() {
                $(this).find('.js-upload-button').attr("src", "/static/pasien_rawat/svg/upload.svg")
            })
        })
        $(".js-pasien-rawat-form").submit(function (e) {
            // Mencegah halaman melakukan reload
            e.preventDefault();
            // Sebelum di kirimkan ke views.py untuk dibuatkan modelnya, perlu di buatkan objek FormData, berhubung
            // data form ini terdapat file, jika tidak ada file, cukup serialize saja
            var pasienRawat_formData = new FormData(this);
            $.ajax({
                url     : $(this).attr("action"),
                type    : 'POST',
                data    : pasienRawat_formData,
                success : function (response) {
                    var pasienRawat = JSON.parse(response["pasien_rawat"]);
                    var fields = pasienRawat[0]["fields"];
                    alert(fields["nama"] + " telah terdaftar sebagai pasien rawat.");
                },
                error   : function (response) {
                    alert("Data yang anda masukkan tidak valid");
                },
                cache: false,
                contentType: false,
                processData: false
            })
        })
    }
);
