

$(document).ready(function(){
    // $("#button").click(function(){
    //     alert("Submit Berhasil");
    // });
    $("form").submit(function(e){
        e.preventDefault();
        $.ajax({
            url: window.location.href, 
            type: "POST", 
            // serialize untuk dapetin data
            data: $(this).serialize(),
            success: function(){
                alert("Submit berhasil");
            },
            fail: function(){
                alert("Submit gagal");
            }
        })
    });
    $.ajax({
        url: window.location.origin+"/datars/js/"+provinsi,
        dataType : "json",
        success: function(datajs){
            console.log(datajs);
            let table = $("#table")
            let result = "" 
            for (let i=0; i < datajs.length; i++ ){
            result += `<tr class = "tabel-border color-1">
            <th>
                <p id = "desc-text" class = "montserrat-regular medium-font padding-1 margin-left-4">Rumah sakit</p>
            </th>
            <th></th>
            </tr>
            <tr>
            <th class = "tabel-border" style = "width: 50%">
                <p id = "desc-text" class = "montserrat-regular medium-font padding-half margin-left-4">Nama</p>
            </th>
            <th class = "tabel-border" style = "width: 50%">
                <p id = "desc-text" class = "montserrat-regular medium-font padding-half margin-left-4"> ${datajs[i].fields.nama} </p>
            </th>
            </tr>
            <tr>
            <th class = "tabel-border" style = "width: 50%">
                <p id = "desc-text" class = "montserrat-regular medium-font padding-half margin-left-4">No telepon / HP</p>
            </th>
            <th class = "tabel-border" style = "width: 50%">
                <p id = "desc-text" class = "montserrat-regular medium-font padding-half margin-left-4">${datajs[i].fields.nohp}</p>
            </th>
            </tr>
            <tr>
            <th class = "tabel-border" style = "width: 50%">
                <p id = "desc-text" class = "montserrat-regular medium-font padding-half margin-left-4">Alamat</p>
            </th>
            <th class = "tabel-border" style = "width: 50%">
                <p id = "desc-text" class = "montserrat-regular medium-font padding-half margin-left-4">${datajs[i].fields.alamat}</p>
            </th>
            </tr>`
            }  
            table.html(result);
            console.log(result);

        }
    })
});