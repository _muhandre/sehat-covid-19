from django import forms
from .models import ModelRS

PROVINSI = (
    ("Aceh", "Aceh"),
    ("Sumatra Utara", "Sumatra Utara"),
    ("Sumatra Barat", "Sumatra Barat"),
    ("Riau", "Riau"),
    ("Kepulauan Riau", "Kepulauan Riau"),
    ("Jambi", "Jambi"),
    ("Bengkulu", "Bengkulu"),
    ("Sumatra Selatan", "Sumatra Selatan"),
    ("Kepulauan Bangka Belitung", "Kepulauan Bangka Belitung"),
    ("Lampung", "Lampung"),
    ("Banten", "Banten"),
    ("Jawa Barat", "Jawa Barat"),
    ("Jakarta", "Jakarta"),
    ("Jawa Tengah", "Jawa Tengah"),
    ("Yogyakarta", "Yogyakarta"),
    ("Jawa Timur", "Jawa Timur"),
    ("Bali", "Bali"),
    ("Nusa Tenggara Barat", "Nusa Tenggara Barat"),
    ("Kalimantan Barat", "Kalimantan Barat"),
    ("Kalimantan Selatan", "Kalimantan Selatan"),
    ("Kalimantan Tengah", "Kalimantan Tengah"),
    ("Kalimantan Timur", "Kalimantan Timur"),
    ("Kalimantan Utara", "Kalimantan Utara"),
    ("Gorontalo", "Gorontalo"),
    ("Sulawesi Barat", "Sulawesi Barat"),
    ("Sulawesi Selatan", "Sulawesi Selatan"),
    ("Sulawesi Tengah", "Sulawesi Tengah"),
    ("Sulawesi Tenggara", "Sulawesi Tenggara"),
    ("Sulawesi Utara", "Sulawesi Utara"),
    ("Maluku", "Maluku"),
    ("Maluku Utara", "Maluku Utara"),
    ("Papua Barat", "Papua Barat"),
    ("Papua", "Papua"),
)
class rsform(forms.ModelForm):
    class Meta:
        model = ModelRS
        fields = ['nama', 'nohp', 'alamat', 'provinsi']
        
    error_messages = {
        'required': 'This field is required.'
    }
    attrs = {
        'class': 'form-control'
    }
    nama = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Nama RS', max_length=50, required=True)
    nohp = forms.CharField(widget=forms.TextInput(attrs=attrs), label='No HP', max_length=50, required=True)
    alamat = forms.CharField(widget=forms.TextInput(attrs=attrs), label='Alamat', max_length=1000,  required=True)
    provinsi = forms.ChoiceField(choices = PROVINSI)