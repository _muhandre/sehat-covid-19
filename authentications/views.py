from django.shortcuts import render, redirect
from django.contrib.auth.models import User, Group
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import CreateForm
from .decorators import is_logged
from django.http import HttpResponseRedirect

@is_logged
def register(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            user = form.save()
            if form.cleaned_data.get("user_type") == "Hospital":
                try:
                    group = Group.objects.get(name="Hospital")
                    user.groups.add(group)
                except Group.DoesNotExist:
                    group = Group.objects.create(name = "Hospital")
                    user.groups.add(group)
            elif form.cleaned_data.get("user_type") == "Patient":
                try:
                    group = Group.objects.get(name="Patient")
                    user.groups.add(group)
                except Group.DoesNotExist:
                    group = Group.objects.create(name = "Patient")
                    user.groups.add(group)
            name = form.cleaned_data.get('username')
            messages.success(request, 'Account: ' + name + ' has been created')
        else:
            messages.error(request, "Invalid data input")
        return redirect("/register")
    else:
        form = CreateForm()
        return render(request, 'authentications/registration.html', {"form": form})