from django.db import models
from django.db.models import Model 

# Create your models here.
class Testimoni(models.Model):
    display_name = models.CharField(max_length = 35, default = "Anonymous")
    content = models.TextField(max_length = 500)