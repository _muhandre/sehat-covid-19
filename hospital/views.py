from django.shortcuts import render, redirect
from pasien_rawat.models import PasienRawat
from pasien_nonrawat.models import PatientNonRawat
from django.http import JsonResponse, HttpResponseRedirect
from django.core import serializers
from django.contrib.auth.decorators import login_required
from authentications.decorators import allowed

# Create your views here.
@login_required(login_url='/login')
@allowed(roles=['Hospital'])
def index(request):
    context = {}
    return render(request, 'hospital/hospital.html', context)

@login_required(login_url='/login')
@allowed(roles=['Hospital'])
def rspasienrawat(request):    
    return render(request, 'hospital/rspasienrawat.html', context = {})

@login_required(login_url='/login')
@allowed(roles=['Hospital'])
def getrspasienrawat(request):
    if request.is_ajax and request.method == 'GET':
        semua_pasien = PasienRawat.objects.all()
        list_nama_rs = []
        for pasien in semua_pasien:
            if pasien.nama_rumah_sakit.capitalize() not in list_nama_rs:
                list_nama_rs.append(pasien.nama_rumah_sakit.capitalize())
        context = {
            'list_nama_rs' : list_nama_rs
        }
        return JsonResponse(context)
    else:
        return JsonResponse({}, status = 400)

@login_required(login_url='/login')
@allowed(roles=['Hospital'])
def daftarpasienrawat(request, nama_rs):
    if(nama_rs.capitalize() == request.user.first_name.capitalize()):
        return render(request, 'hospital/daftarpasienrawat.html', context = {})
    else:
        return render(request, 'hospital/rsnotallowed.html', {'pasien_rawat' : True})

@login_required(login_url='/login')
@allowed(roles=['Hospital'])
def getdaftarpasienrawat(request, nama_rs):
    if request.is_ajax and request.method == "GET":
        semua_pasien = PasienRawat.objects.all()
        pasiennya_nama_rs = []
        for pasien in semua_pasien:
            if pasien.nama_rumah_sakit.capitalize() == nama_rs:
                pasiennya_nama_rs.append(
                    {   
                        "nama"              :   pasien.nama,
                        "nomor_telepon"     :   pasien.nomor_telepon,
                        "alamat"            :   pasien.alamat,
                        "nama_rumah_sakit"  :   pasien.nama_rumah_sakit,
                        "base64_doc"        :   pasien.base64_doc
                    }
                )
        context = {
            'pasiennya_nama_rs' : pasiennya_nama_rs
        }
        return JsonResponse(context)
    else:
        return JsonResponse({}, status = 400)



@login_required(login_url='/login')
@allowed(roles=['Hospital'])
def rspasienperiksa(request):
    return render(request, 'hospital/rspasienperiksa.html', context = {})

@login_required(login_url='/login')
@allowed(roles=['Hospital'])
def getrspasienperiksa(request):
    if request.is_ajax and request.method == 'GET':
        all_patient = PatientNonRawat.objects.all()
        hospital_list = []
        for patient in all_patient:
            if patient.hospital_name.capitalize() not in hospital_list:
                hospital_list.append(patient.hospital_name.capitalize())
        context = {
            'list_nama_rs' : hospital_list
        }
        return JsonResponse(context)
    else:
        return JsonResponse({}, status = 400)

@login_required(login_url='/login')
@allowed(roles=['Hospital'])
def daftarpasienperiksa(request, hospital):
    if(hospital.capitalize() == request.user.first_name.capitalize()):
        return render(request, 'hospital/daftarpasienperiksa.html', context = {})
    else:
        return render(request, 'hospital/rsnotallowed.html', {'pasien_rawat' : False})

@login_required(login_url='/login')
@allowed(roles=['Hospital'])
def getdaftarpasienperiksa(request, nama_rs):
    if request.is_ajax and request.method == "GET":
        all_patient = PatientNonRawat.objects.all()
        hospital_patient = []
        for patient in all_patient:
            if patient.hospital_name.capitalize() == nama_rs:
                hospital_patient.append(
                    {   
                        "patient_name"      :   patient.patient_name,
                        "patient_phone"     :   patient.patient_phone,
                        "patient_address"   :   patient.patient_address,
                        "hospital_name"     :   patient.hospital_name,
                        "symptoms"          :   patient.symptoms
                    }
                )
        context = {
            'pasiennya_nama_rs' : hospital_patient
        }
        return JsonResponse(context)
    else:
        return JsonResponse({}, status = 400)