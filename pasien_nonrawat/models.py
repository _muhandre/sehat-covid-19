from django.db import models
from datars.models import ModelRS

# Create your models here.
class PatientNonRawat(models.Model):
    patient_name = models.CharField(max_length = 60)
    patient_phone = models.CharField(max_length = 20)
    patient_address = models.CharField(max_length = 500)
    hospital_name = models.CharField(max_length = 100)
    symptoms = models.TextField(max_length = 1000)
    hospital = models.ForeignKey(ModelRS, on_delete = models.CASCADE)
