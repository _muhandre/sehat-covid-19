# Sehat COVID-19

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/_muhandre/sehat-covid-19/master?label=pipeline)
[![coverage report](https://gitlab.com/_muhandre/sehat-covid-19/badges/master/coverage.svg)](https://gitlab.com/_muhandre/sehat-covid-19/-/commits/master)

#### Kelompok D-15
###### Anggota kelompok
- Muhammad Raffy Suprajeni
NPM : 1906399732
- Nurdiana Putri
NPM : 1906399871
- Ruly Achmad Gemilang Gultom
NPM : 1906399915
- Fauzan Ramadhanto Andiko Putra
NPM : 1906400015
- Muhamad Andre Gunawan
NPM : 1906400021

Link Herokuapp : [http://sehat-covid-19.herokuapp.com/](http://sehat-covid-19.herokuapp.com/)
  
Link wireframe: [https://www.figma.com/file/axE3qk7uR08N2nfdp0Qu7W/D-15-TK-1-PPW-2020-WireFrame?node-id=0%3A1](https://www.figma.com/file/axE3qk7uR08N2nfdp0Qu7W/D-15-TK-1-PPW-2020-WireFrame?node-id=0%3A1)

Link prototype: [https://www.figma.com/proto/SoN5wYmSyUy1vQWxwRm22t/D---15-TK-1-PPW-2020?node-id=35%3A49&scaling=scale-down](https://www.figma.com/proto/SoN5wYmSyUy1vQWxwRm22t/D---15-TK-1-PPW-2020?node-id=35%3A49&scaling=scale-down)

Link gitlab: [https://gitlab.com/_muhandre/sehat-covid-19](https://gitlab.com/_muhandre/sehat-covid-19)


#### Manfaat aplikasi Sehat COVID-19
Aplikasi ini akan membantu orang-orang yang terindikasi positif maupun yang baru saja mengalami gejala, untuk dapat mendapatkan perawatan intensif dengan segera, tanpa perlu mengkhawatirkan akan masalah transportasi.

Bagi orang-orang seperti itu, terkadang mereka merasa sedikit kebingungan untuk pergi ke rumah sakit maupun klinik dikarenakan transportasi apa yang mereka gunakan untuk pergi kesana. Jika mereka menggunakan transportasi pribadi, ataupun menggunakan tranpsortasi umum, mereka khawatir akan mengakibatkan orang lain terindikasi.

Maka dari itu, dengan kerja sama berbagai belah pihak, aplikasi kami hadir sebagai tawaran solusi akan masalah tersebut, dimana kami menawarkan jasa antar ke rumah-sakit terdekat menggunakan ambulans.

#### Fitur aplikasi Sehat COVID-19
- Jasa antar jemput pasien yang meminta rawat inap.
- Jasa antar jemput pasien yang meminta pemeriksaan(test) COVID-19.
- Pendaftaran terbuka bagi rumah sakit yang ingin turut berkontribusi memberikan layanan antar jemput.

Copyright@2020
