from django import forms
from .models import PasienRawat

class PasienRawatForm(forms.ModelForm):
    class Meta:
        model = PasienRawat
        fields = ['nama', 'nomor_telepon', 'alamat', 'nama_rumah_sakit', 'dokumen_hasil_test']
        widgets = {
            'nama' : forms.TextInput(attrs = {'class': 'form-control'}),
            'nomor_telepon' : forms.TextInput(attrs = {'class': 'form-control'}),
            'alamat' : forms.TextInput(attrs = {'class': 'form-control'}),
            'nama_rumah_sakit' : forms.TextInput(attrs = {'class': 'form-control'}),
            'dokumen_hasil_test' : forms.FileInput(attrs = {'id' : 'file-input', 'class': 'form-control'}),
        }